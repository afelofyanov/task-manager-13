package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();
}
