package ru.tsc.felofyanov.tm.controller;

import ru.tsc.felofyanov.tm.api.controller.IProjectTaskController;
import ru.tsc.felofyanov.tm.api.service.IProjectTaskService;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
    }
}
