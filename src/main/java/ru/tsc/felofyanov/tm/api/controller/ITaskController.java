package ru.tsc.felofyanov.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void showTaskListByProjectId();

    void clearTasks();

    void createTask();

    void removeTaskByIndex();

    void removeTaskById();

    void showTaskByIndex();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();
}
